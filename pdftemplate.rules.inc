<?php

/**
 * Implement hook_rules_action_info().
 */
function pdftemplate_rules_action_info() {
  $templates = pdftemplate_template_load_all();

  $actions = array();

  foreach($templates as $template) {
    $vars = array();
    $vars['template_machine_name'] = array(
      'type' => 'pdftemplate_settings',
      'restriction' => 'input',
      'label' => t('Template settings'),
      'machine_name' => $template->machine_name,
    );
    $vars['destination_uri'] = array(
      'label' => t('Destination URI'),
      'type' => 'text',
      'options list' => 'pdftemplate_rules_destination_rui_options',
      'restriction' => 'input',
    );
    $vars['destination_folder'] = array(
      'label' => t('Destination folder'),
      'type' => 'text',
      'optional' => true
    );
    $vars['destination_filename'] = array(
      'label' => t('Filename'),
      'type' => 'text',
    );
    if(isset($template->data['variables'])) {
      foreach($template->data['variables'] as $var) {
        $vars[$var['variable']] = array(
          'type' => $var['type'],
          'label' => $var['description'],
          'optional' => true
        );
      }
    }
    
    $actions['pdftemplate_generate_' . $template->machine_name] = array(
      'label' => t('Create PDF with template "@template"', array('@template' => $template->name)),
      'group' => t('PDFTemplate'),
      'base' => 'pdftemplate_rules_generate',
      'named parameter' => true,
      'parameter' => $vars,
      'provides' => array(
        'pdf' => array(
          'type' => 'file',
          'label' => t('PDF'),
        ),
      ),
    );
  }

  $actions['pdftemplate_rules_download'] = array(
    'label' => t('Download PDF'),
    'group' => t('PDFTemplate'),
    'parameter' => array(
      'pdf' => array(
        'type' => 'file',
        'label' => t('PDF'),
      )
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_data_info().
 */
function pdftemplate_rules_data_info() {
  $data['pdftemplate_settings'] = array(
    'label' => t('PDFTemplate settings'),
    'ui class' => 'RulesDataUIPDFTemplateSettings',
  );
  return $data;
}

/**
 * Adds a payment method settings form to the enabling action.
 */
class RulesDataUIPDFTemplateSettings extends RulesDataUI implements RulesDataDirectInputFormInterface {
  public static function getDefaultMode() {
    return 'input';
  }

  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    if(!empty($info['machine_name']) && $template = pdftemplate_template_load($info['machine_name'])) {
      $form[$name]['machine_name'] = array('#type' => 'value', '#value' => $info['machine_name']);
      $form[$name]['settings']['no_settings']['#markup'] = t('Selected template: @template_name', array('@template_name' => $template->name));
    }
    else {
      $form[$name]['invalid']['#markup'] = t('Invalid or missing template');
    }
    return $form;
  }

  public static function render($value) {
    return array();
  }
}

function pdftemplate_rules_generate($params) {
  if($template = pdftemplate_template_load($params['template_machine_name']['machine_name'])) {
    $pdf = new PDFTemplate();
    $pdf->setTemplate($params['template_machine_name']['machine_name']);
    $pdf->setDestination($params['destination_uri']);
    $pdf->setFilename($params['destination_folder'] . '/' . $params['destination_filename']);

    if(isset($template->data['variables'])) {
      foreach($template->data['variables'] as $var) {
        if(isset($params[$var['variable']])) {
          if($var['type'] == 'text') {
            $pdf->addVar($var['variable'], $params[$var['variable']]);
          }
          if($var['type'] == 'file') {
            $pdf->addImage($var['variable'], $params[$var['variable']]->uri);
          }
        }
      }
    }

    if($file = $pdf->generatePDF()) {
      return array('pdf' => $file);
    }
  }
}

function pdftemplate_rules_download($pdf) {
  if(isset($pdf->uri)) {
    $headers = file_get_content_headers($pdf);
    file_transfer($pdf->uri, $headers);
  }
}

function pdftemplate_rules_destination_rui_options() {
  return array(
    'temporary://' => 'temporary://',
    'private://' => 'private://',
    'public://' => 'public://'
  );
}