<?php

/**
 * Implementation of hook_schema
 * @return mixed
 */
function pdftemplate_schema() {
  $schema['pdftemplate'] = array(
    'description' => 'Stores template data.',
    'fields' => array(
      'machine_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The vocabulary machine name.',
      ),
      'name' => array(
        'description' => 'The title of this template.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'data' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data.',
      ),
      'fid' => array(
        'description' => 'The file id.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'machine_name' => array('machine_name'),
    ),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function pdftemplate_install() {
  if(variable_get('file_private_path')) {
    $destination = 'private://pdftemplates';
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
  }
}

/**
 * Implements hook_uninstall().
 */
function pdftemplate_uninstall() {
  // Remove all variables.
  db_delete('variable')
    ->condition('name', 'pdftemplate_%', 'like')
    ->execute();
}

/**
 * Implements hook_requirements().
 */
function pdftemplate_requirements($phase) {
  $requirements = array();

  if($phase == 'runtime') {
    $directory = variable_get('pdftemplate_destination', 'private://') . variable_get('pdftemplate_destination_path', 'pdftemplates');
    if(file_prepare_directory($directory)) {
      $serverity = REQUIREMENT_OK;
      $description = t('Template path exists.');
      $value = variable_get('pdftemplate_destination', 'private://') . variable_get('pdftemplate_destination_path', 'pdftemplates');
    } else {
      $serverity = REQUIREMENT_ERROR;
      $description = t('Template path does not exist. Please check your ' . l(t('settings'), 'admin/config/services/pdftemplate/settings') . ' and create the path to store the template files.');
      $value = t('N/A');
    }
    $requirements['pdftemplate_path'] = array(
      'title' => t('PDFTemplate path'),
      'description' => $description,
      'severity' => $serverity,
      'value' => $value,
    );
  }

  return $requirements;
}

