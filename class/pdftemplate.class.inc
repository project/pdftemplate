<?php

class PDFTemplate {
  public $settings = NULL;
  public $vars = array();
  public $images = array();
  public $destination = NULL;
  public $filename = NULL;
  public $file = NULL;
  public $template = array();
  public $rows = array();

  public function setUser($user) {
    $this->settings['user'] = $user;
  }

  public function setTemplate($template_machine_name) {
    $template = pdftemplate_template_load($template_machine_name);

    $file = file_load($template->fid);
    $file_data = file_get_contents($file->uri);

    $this->template = array(
      'extension' => 'odt',
      'base64' => base64_encode($file_data),
    );
  }

  public function setRemoteTemplateName($template_name) {
    $this->template = $template_name;
  }

  public function setKey($key) {
    $this->settings['key'] = $key;
  }

  public function setFilename($filename) {
    $this->filename = $filename;
  }

  public function setDestination($destination) {
    $this->destination = $destination;
  }

  public function addVar($name, $value) {
    $this->vars[$name] = $value;
  }

  public function addRow(PDFTemplateRow $row) {
    $this->rows[$row->rowName][] = $row->row[$row->rowName];
  }

  public function addImage($name, $image_url) {
    if($image = file_get_contents($image_url)) {
      $path_info = pathinfo($image_url);

      $extention = $path_info['extension'];
      $extension = substr($extention, 0, strpos($extention, '?'));

      $this->vars[$name] = array(
        'type' => 'image',
        'extension' => $extension,
        'base64' => base64_encode($image)
      );

      $data = base64_decode(base64_encode($image));
    }
  }

  public function getStatus() {
    $result = $this->sendRequest('status', array());
  }

  public function generatePDF() {
    $this->settings['return'] = 'base64';

    $data = $this->settings;
    $data['vars'] = array_merge($this->vars, $this->rows);
    $data['template'] = $this->template;

    foreach($this->images as $key => $image) {
      $data[$key] = array(
        'type' => 'image',
        'extension' => $image['extension'],
        'base64' => $image['base64'],
      );
    }

    $result = $this->sendRequest('job', $data);

    if($result->status == 200) {
      $pdf_decoded = base64_decode($result->base64);

      $destination = $this->destination . $this->filename;
      // what to do when the destination file already exists
      $replace = FILE_EXISTS_REPLACE;
      $file = file_save_data($pdf_decoded, $destination, $replace);

      $this->file = $file;

      return $file;
    } else {
      watchdog('pdftemplate', 'Error: @error', array('@error' => $result->message));
    }
  }

  private function sendRequest($method, $data) {
    $data_string = json_encode($data);

    $ch = curl_init('http://pdftemplate.eu/api/v2/' . $method);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);
    $result = json_decode($result);
    return $result;
  }
}

class PDFTemplateRow {
  public $rowName = FALSE;
  public $row = array();

  function __construct($rowName) {
    $this->rowName = $rowName;
  }

  public function addVar($name, $value) {
    $this->row[$this->rowName][$name] = $value;
  }
}