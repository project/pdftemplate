<?php

function pdftemplate_manage_vars($machine_name) {
  $template = pdftemplate_template_load($machine_name);

  $output = _pdftemplate_vars_table($template);
  $output .= '<hr />';

  $form = drupal_get_form('pdftemplate_vars_form', $machine_name);
  $output .= drupal_render($form);

  return $output;
}

function _pdftemplate_vars_table($template) {
  $rows = array();

  if(isset($template->data['variables'])) {
    foreach($template->data['variables'] as $var) {
      $rows[] = array(
        'variable' => '{' . $var['variable'] . '}',
        'type' => $var['type'],
        'description' => $var['description']
      );
    }
  }

  $vars = array(
    'header' => array(
      t('Variable'), t('Type'), t('Description')
    ),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => '',
    'empty' => t('No template variables specified yet.'),
  );

  return theme_table($vars);
}

function pdftemplate_vars_form($form, $form_state, $machine_name) {
  $form['machine_name'] = array(
    '#type' => 'hidden',
    '#value' => $machine_name
  );

  $form['variable'] = array(
    '#type' => 'textfield',
    '#title' => t('Variable name'),
    '#required' => true,
    '#field_prefix' => '{',
    '#field_suffix' => '}',
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#required' => true,
    '#options' => _pdftemplate_get_var_types()
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#required' => true,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add variable'),
  );

  return $form;
}

function pdftemplate_vars_form_submit($form, $form_state) {
  $template = pdftemplate_template_load($form_state['values']['machine_name']);
  $template->data['variables'][$form_state['values']['variable']] = array(
    'variable' => $form_state['values']['variable'],
    'type' => $form_state['values']['type'],
    'description' => $form_state['values']['description']
  );

  pdftemplate_template_save($template);
}

function _pdftemplate_get_var_types() {
  return array(
    'text' => t('Text'),
    'file' => t('Image')
  );
}