<?php

function pdftemplate_page() {
  // drupal_set_message(t('PDFTemplate path to store ODT template files is either not set or not accessible. Please check your settings.'), 'error');

  $templates = pdftemplate_template_load_all();

  $rows = array();
  foreach($templates as $template) {
    $file = file_load($template->fid);

    $rows[] = array(
      'name' => $template->name,
      'machine_name' => $template->machine_name,
      'file' => l($file->filename, file_create_url($file->uri)),
      'vars' => l(t('Edit variables'), 'admin/config/services/pdftemplate/' . $template->machine_name . '/vars'),
      'edit' => l(t('Edit'), 'admin/config/services/pdftemplate/' . $template->machine_name . '/edit', array('query' => drupal_get_destination())),
      'delete' => l(t('Delete'), 'admin/config/services/pdftemplate/' . $template->machine_name . '/delete', array('query' => drupal_get_destination())),
    );
  }

  $vars = array(
    'header' => array(
      t('Name'), t('Machine name'), t('File'), '', '', ''
    ),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => '',
    'empty' => '',
  );

  return theme_table($vars);
}

function pdftemplate_form($form, $form_state, $machine_name = FALSE) {
  if($machine_name) {
    $form['update'] = array(
      '#type' => 'hidden',
      '#value' => 'update'
    );
    $template = pdftemplate_template_load($machine_name);
  }

  $form['name'] = array(
    '#title' => t('Name of template'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => (isset($template->name)) ? $template->name : '',
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => (isset($template->machine_name)) ? $template->machine_name : '',
    '#machine_name' => array(
      'exists' => 'pdftemplate_template_load',
      'source' => array('name')
    ),
  );

  if(isset($template->machine_name)) {
    $form['machine_name']['#disabled'] = true;
  }

  $form['template_file'] = array(
    '#title' => t('Template file'),
    '#description' => t('Select template file in open document format (.odt)'),
    '#type' => 'file',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function pdftemplate_form_validate($form, &$form_state) {
  $destination = variable_get('pdftemplate_destination', 'private://') . variable_get('pdftemplate_destination_path', 'private://');
  $file = file_save_upload('template_file', array(
    'file_validate_extensions' => array('odt'),
  ), $destination, FILE_EXISTS_RENAME);
  // If the file passed validation:
  if ($file) {
    $form_state['storage']['file'] = $file;
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}

function pdftemplate_form_submit($form, &$form_state) {
  // Get file object
  $file = $form_state['storage']['file'];
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);

  // Save
  $record = array(
    'machine_name' => $form_state['values']['machine_name'],
    'name' => $form_state['values']['name'],
    'fid' => $file->fid,
  );

  if(isset($form_state['values']['update'])) {
    drupal_write_record('pdftemplate', $record, 'machine_name');
  } else {
    drupal_write_record('pdftemplate', $record);
  }

  drupal_goto('admin/config/services/pdftemplate');
}

function pdftemplate_settings_form($form, &$form_state) {
  $form['pdftemplate_destination'] = array(
    '#type' => 'radios',
    '#title' => t('Template location'),
    '#options' => array(
      'public://' => t('Public file directory'),
      'private://' => t('Private file directory')
    ),
    '#default_value' => variable_get('pdftemplate_destination', 'private://'),
    '#description' => t('Attention: it is strongly recommented to store templates in the private file system.')
  );

  $form['pdftemplate_destination_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => variable_get('pdftemplate_destination_path', 'pdftemplates'),
  );

  $form['pdftemplate_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $form['pdftemplate_advanced']['pdftemplate_account'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use PDFTemplate account'),
    '#description' => t('Use your PDFTemplate.eu account to generate PDF files (not required).'),
    '#default_value' => variable_get('pdftemplate_account', FALSE),
  );

  $form['pdftemplate_advanced']['pdftemplate_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('pdftemplate_user', ''),
    '#states' => array(
      'disabled' => array(
        ':input[name="pdftemplate_account"]' => array('checked' => FALSE),
      )
    )
  );

  $form['pdftemplate_advanced']['pdftemplate_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('pdftemplate_key', ''),
    '#states' => array(
      'disabled' => array(
        ':input[name="pdftemplate_account"]' => array('checked' => FALSE),
      )
    )
  );

  $form = system_settings_form($form);

  return $form;
}

function pdftemplate_settings_form_validate($form, &$form_state) {
  $destination = $form_state['input']['pdftemplate_destination'] . $form_state['input']['pdftemplate_destination_path'];
  if(!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
    form_set_error('pdftemplate_destination_path', t('The selected folder does not exist and cannot be created.'));
  }
}

function pdftemplate_delete_confirmation_form($form, $form_state, $machine_name) {
  $form['machine_name'] = array(
    '#type' => 'hidden',
    '#value' => $machine_name
  );

  $form['markup'] = array(
    '#markup' => t('Are you sure?')
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete template')
  );

  return $form;
}
function pdftemplate_delete_confirmation_form_submit($form, $form_state, $machine_name) {
  pdftemplate_template_delete($form_state['values']['machine_name']);
}